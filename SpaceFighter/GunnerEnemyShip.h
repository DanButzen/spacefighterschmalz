
#pragma once

#include "EnemyShip.h"

class GunnerEnemyShip : public EnemyShip
{

public:

	GunnerEnemyShip();
	virtual ~GunnerEnemyShip() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;

	

};
